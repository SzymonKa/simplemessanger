﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Threading;


namespace SimpleMessanger
{
    public partial class Form1 : Form
    {

        delegate void SetTextCallback(string text);
        private void SetText(string text)
        {
            
            if (this.listBox2.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.listBox2.Items.Add(text);
            }
        }
        private void ThreadProcSafe(string text)
        {
            this.SetText(text);
        }

        private int port { get; set; }
        private IPEndPoint zdalnyIP { get; set; }
        private UdpClient serwer { get; set; }


        private void Host(UdpClient ser, IPEndPoint zdalny_IP)
        {
            
            try{
                               
                Byte[] odczyt = ser.Receive(ref zdalny_IP);

                string dane = Encoding.ASCII.GetString(odczyt);

              
                ThreadProcSafe("Odebrano:"+dane);
               
                }

            catch(Exception ex){

                MessageBox.Show(ex.ToString(), "Błąd");
                return;
                }
            Host(ser, zdalny_IP);
            return;
        }

        

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            string host = textBoxIPSend.Text;
            int port = System.Convert.ToInt16(numericUpDownSend.Value);

            try
            {

                UdpClient klient = new UdpClient(host, port);
                Byte[] dane = Encoding.ASCII.GetBytes(textBoxMessSend.Text);
                klient.Send(dane, dane.Length);
                listBox1.Items.Add("Wyslanie wiadomosci do hosta " + host + ":" + port);
                listBox2.Items.Add("Wyslano: " + textBoxMessSend.Text);
                klient.Close();

            }
            catch (Exception ex)
            {
                listBox1.Items.Add("Blad:nie udalo sie wyslac wiadomosci!");
                MessageBox.Show(ex.ToString(), "Blad");
            }

        }

        private  void buttonStart_Click(object sender, EventArgs e)
        {
             port= System.Convert.ToInt16(numericUpDownHost.Value);

             zdalnyIP = new IPEndPoint(IPAddress.Any, 0);

             serwer = new UdpClient(port);

             listBox1.Items.Add("Server uruchomiony");

             Task  serv =  Task.Run(() =>
              {
                Host(serwer, zdalnyIP);
              });

             buttonStart.Enabled = false;
             buttonStop.Enabled = true;
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            serwer.Close();
            listBox1.Items.Add("Server wylaczony");
            buttonStart.Enabled = true;
            buttonStop.Enabled = false;
        }

        private void howToToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HowTo how = new HowTo();
            how.Show();

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.Show();
        }
    }
}
